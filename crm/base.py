from query import SelectQuery, UpdateQuery, InsertQuery
from six import iteritems
from bson import ObjectId


class BaseApiModel(object):
    def __init__(self, *args, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def as_dict(self):
        result = {}

        for attr, _ in iteritems(self._meta.attribute_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.as_dict() if hasattr(x, "as_dict") else x,
                    value
                ))
            elif hasattr(value, "id"):
                result[attr] = str(value.id)
            elif isinstance(value, ObjectId):
                result[attr] = str(value)
            elif hasattr(value, "as_dict"):
                result[attr] = value.as_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].as_dict())
                    if hasattr(item[1], "as_dict") else item,
                    value.items()
                ))
            elif value is not None:
                result[attr] = value

        return result

    @classmethod
    def get_field_by_name(cls, name):
        if name in cls._meta.attribute_types:
            return cls._meta.attribute_types[name]
        return {'type': 'str', 'required': False}
        # raise AttributeError('Field named %s not found' % name)

    def save(self):
        if self.get_pk():
            update = self.update(
                self.get_pk(),
                **self.as_dict()
            )
            result = update.execute()
        else:
            insert = self.insert(**self.as_dict())
            result = insert.execute()

        for key, value in result.items():
            setattr(self, key, value)

        return result

    @classmethod
    def select(cls, *args, **kwargs):
        return SelectQuery(cls, *args, **kwargs)

    @classmethod
    def create(cls, **query):
        inst = cls(**query)
        inst.save()
        return inst

    @classmethod
    def update(cls, reference, **query):
        return UpdateQuery(cls, reference, **query)

    @classmethod
    def insert(cls, **query):
        return InsertQuery(cls, **query)

    @classmethod
    def get(cls, *args, **kwargs):
        return cls.select().get(*args, **kwargs)

    def one(self, resource_model):
        return resource_model.select().get(self.get_pk(),
                                           resource_model=self.__class__)

    def list(self, resource_model, **kwargs):
        return resource_model.select().list(self.get_pk(),
                                            self.__class__,
                                            **kwargs)

    @classmethod
    def all(cls, *args, **kwargs):
        return cls.select().all(*args, **kwargs)

    def get_pk(self):
        return getattr(self, self._meta.pk_name, None)

    def get_field_dict(self):
        def get_field_val(field):
            field_value = getattr(self, field.name)
            if not self.get_pk() and field_value is None and field.default is not None:
                if callable(field.default):
                    field_value = field.default()
                else:
                    field_value = field.default
                setattr(self, field.name, field_value)
            return (field.name, field_value)

        pairs = map(get_field_val, self._meta.fields.values())

        return dict(pairs)

