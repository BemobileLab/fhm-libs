import six
from api import APIRequest
from exceptions import DoesNotExist


class BaseQuery(object):
    def __init__(self, model, method=None):
        self.model = model
        self.method = method
        self._api = APIRequest.init_app()

    @property
    def api(self):
        return self._api

    def parse_result(self, result):
        if not result:
            return None
        data = result.get('data')
        return data

    def parse_url(self, meta_url, params=None):
        if isinstance(meta_url, dict):
            url = meta_url.get(self.identifier)
        else:
            url = meta_url

        if params:
            url = url % params

        return url


class SelectQuery(BaseQuery):
    identifier = 'SELECT'

    def __init__(self, model, *args, **kwargs):
        super(SelectQuery, self).__init__(model, 'GET')

    def get(self, reference,  **kwargs):
        meta_url = self.parse_url(self.model._meta.url, kwargs)
        url = '%s/%s' % (meta_url, reference)

        result, data = self.api.request(self.method, url)

        if not data:
            if result.status_code == 404:
                raise DoesNotExist('instance %s matching reference %s does not exist' % (self.model.__name__,
                                                                                               reference))
            else:
                return self.api._create_apierror(result, url)

        kwargs = dict(self.parse_result(data))
        return self.model(**kwargs)

    def list(self, reference, resource_model, **kwargs):        
        result, data = self.api.request(self.method,
                                       '/%s/%s/%s' % (resource_model._meta.verbose_name_plural, reference,
                                                      self.model._meta.verbose_name_plural), **kwargs)

        return [self.model(**dict(entry)) for entry in self.parse_result(data)]

    def all(self, **params):        
        url = self.parse_url(self.model._meta.url, params)
        result, data = self.api.request(self.method, url, **params)

        if 'errors' in data:
            return self.api._create_apierror(result, url)

        return [self.model(**dict(entry)) for entry in self.parse_result(data)]


class InsertQuery(BaseQuery):
    identifier = 'INSERT'

    def __init__(self, model, **kwargs):
        self.insert_query = kwargs
        super(InsertQuery, self).__init__(model, 'POST')

    def parse_insert(self):
        pairs = {}
        for k, v in six.iteritems(self.insert_query):
            field = self.model.get_field_by_name(k)

            if field['required'] or v is not None:
                pairs[k] = v

        return pairs

    def execute(self):
        data = self.parse_insert()
        url = self.parse_url(self.model._meta.url, self.insert_query)
        result, data = self.api.request(self.method, url, data=data)

        return dict(self.parse_result(data))


class UpdateQuery(BaseQuery):
    identifier = 'UPDATE'

    def __init__(self, model, reference, **kwargs):
        self.update_query = kwargs
        self.reference = reference
        super(UpdateQuery, self).__init__(model, 'PUT')

    def parse_update(self):
        pairs = {}
        for k, v in six.iteritems(self.update_query):
            field = self.model.get_field_by_name(k)

            if field['required'] or v is not None:
                pairs[k] = v

        return pairs

    def execute(self):

        data = self.parse_update()

        meta_url = self.parse_url(self.model._meta.url, self.update_query)
        url = '%s/%s' % (meta_url, self.reference)
        result, data = self.api.request(self.method,
                                       url,
                                       data=data)

        return self.parse_result(data)


class ActionQuery(BaseQuery):

    def __init__(self, model, reference, identifier, method='PUT', params=None, **kwargs):
        self.action_query = kwargs
        self.reference = reference
        self.identifier = identifier
        self.params = params
        super(ActionQuery, self).__init__(model, method)

    def execute(self):

        data = self.action_query
        url = self.parse_url(self.model._meta.url, self.params)
        url = url % {'id': self.reference}

        result, data = self.api.request(self.method, url, data=data)
        return self.parse_result(data)
