def normalize_string(unicode_string):
    import unicodedata
    return unicodedata.normalize('NFD', unicode_string).encode('ascii', 'ignore')