# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from bson.objectid import ObjectId
from singleton import Singleton
import requests
from . import Config, get_token
from exceptions import APIError, DecodeError
from requests.exceptions import ConnectionError, Timeout
try:
    import urllib.parse as urlrequest
except ImportError:
    import urllib as urlrequest
try:
    import simplejson as json
except ImportError:
    import json


requests_session = requests.Session()


@Singleton
class APIRequest(object):

    def request(self, method, url, data=None, retries=0, **params):
        params = params or {}

        headers = {
            'Authorization': Config.token,
            'Content-Type': 'application/json'
        }

        encoded_params = urlrequest.urlencode(params)
        absolute_url = self._absolute_url(url, encoded_params)

        json_data = None
        if data or data == {}:
            json_data = json.dumps(data, cls=ComplexEncoder)

        try:
            result = requests_session.request(method, absolute_url, data=json_data, headers=headers)
        except ConnectionError as e:
            msg = '{}'.format(e)

            if msg:
                msg = '%s: %s' % (type(e).__name__, msg)
            else:
                msg = type(e).__name__

            raise APIError(msg)

        except Timeout as e:
            msg = '{}'.format(e)

            if msg:
                msg = '%s: %s' % (type(e).__name__, msg)
            else:
                msg = type(e).__name__

            raise APIError(msg)

        if result.status_code == requests.codes.unauthorized and retries == 0:
            Config.token = get_token()
            return self.request(method, url, data=data, retries=retries+1, **params)

        elif result.status_code not in (requests.codes.ok, requests.codes.not_found,
                                      requests.codes.created, requests.codes.accepted,
                                      requests.codes.no_content):
            self._create_apierror(result, url=absolute_url, data=json_data, method=method)
        elif result.status_code == requests.codes.no_content or result.status_code == requests.codes.not_found:
            return result, None
        else:
            if result.content:
                try:
                    content = result.content
                    return result, json.loads(content)
                except ValueError:
                    self._create_decodeerror(result, url=absolute_url)
            else:
                self._create_decodeerror(result, url=absolute_url)

    def _absolute_url(self, url, encoded_params):
        pattern = '%s%s%s'

        if encoded_params:
            pattern = '%s%s?%s'

        return pattern % (Config.base_url, url, encoded_params)

    def _create_apierror(self, result, url=None, data=None, method=None):
        text = result.text if hasattr(result, 'text') else result.content
        status_code = result.status_code
        headers = result.headers

        try:
            content = result.json()
        except ValueError:
            content = None

        raise APIError(text, code=status_code, content=content, headers=headers, url=url)

    def _create_decodeerror(self, result, url=None):
        text = result.text if hasattr(result, 'text') else result.content
        status_code = result.status_code
        headers = result.headers

        try:
            content = result.json()
        except ValueError:
            content = None

        raise DecodeError(text, code=status_code, headers=headers, content=content, url=url)


class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)