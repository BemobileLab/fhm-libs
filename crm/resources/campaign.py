from ..query import ActionQuery, SelectQuery
from ..base import BaseApiModel


class Campaign(BaseApiModel):
    def __init__(self, id=None, name=None, creation_date=None, start_date=None, end_date=None, status=None, segments=None, actions=None):
        super(Campaign, self).__init__(name=name, creation_date=creation_date, start_date=start_date, end_date=end_date, status=status, segments=segments, actions=actions)
        self._id = id
        self._name = name
        self._creation_date = creation_date
        self._start_date = start_date
        self._end_date = end_date
        self._status = status
        self._segments = segments
        self._actions = actions

    class _meta:
        pk_name = 'id'
        verbose_name = 'campaign'
        verbose_name_plural = 'campaigns'
        url = {
            SelectQuery.identifier: '/mandates',
            'ADD_USER': '/campaigns/$(id)s/segments/users',
            'ADD_USER_TO_SEGMENT': '/campaigns/$(id)s/segments/%(segment_id)s/users'
        }
        attribute_types = {
            'id': {'type': 'str', 'required': False},
            'name': {'type': 'str', 'required': True},
            'creation_date': {'type': 'str', 'required': False},
            'start_date': {'type': 'str', 'required': False},
            'end_date': {'type': 'str', 'required': False},
            'status': {'type': 'str', 'required': False},
            'segments': {'type': 'list', 'required': False},
            'actions': {'type': 'list', 'required': False}
        }

    def add_user(self, user, segment=None):
        action_query = ActionQuery(
            Campaign,
            self.id,
            'ADD_USER_TO_SEGMENT' if segment else 'ADD_USER',
            params={'segment_id': segment.id} if segment else None,
            user_id=user.id,
        )
        return action_query.execute()

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def creation_date(self):
        return self._creation_date

    @creation_date.setter
    def creation_date(self, creation_date):
        self._creation_date = creation_date

    @property
    def start_date(self):
        return self._start_date

    @start_date.setter
    def start_date(self, start_date):
        self._start_date = start_date

    @property
    def end_date(self):
        return self._end_date

    @end_date.setter
    def end_date(self, end_date):
        self._end_date = end_date

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status

    @property
    def segments(self):
        return self._segments

    @segments.setter
    def segments(self, segments):
        self._segments = segments

    @property
    def actions(self):
        return self._actions

    @actions.setter
    def actions(self, actions):
        self._actions = actions
        

    
