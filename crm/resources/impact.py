from ..exceptions import APIError
from ..query import ActionQuery, InsertQuery, SelectQuery
from ..base import BaseApiModel


class Impact(BaseApiModel):
    def __init__(self, id=None, user=None, action=None):
        super(Impact, self).__init__(id=id, user=user, action=action)
        self._id = id
        self._user = user
        self._action = action

    class _meta:
        pk_name = 'id'
        verbose_name = 'impact'
        verbose_name_plural = 'impacts'
        url = {
            InsertQuery.identifier: '/impacts',
        }
        attribute_types = {
            'id': {'type': 'str', 'required': True},
            'user': {'type': 'User', 'required': True},
            'action': {'type': 'PromoAction', 'required': True}
        }

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._user = user

    @property
    def action(self):
        return self._action

    @action.setter
    def action(self, action):
        self._action = action
